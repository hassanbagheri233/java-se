# Pull base image.
FROM openjdk:7

#RUN \
# Update
#apt-get update -y && \
# Install Java
#apt-get install default-jre -y

ADD ./target/first-1.0-SNAPSHOT.jar first-1.0-SNAPSHOT.jar

#EXPOSE 8080

CMD java -cp first-1.0-SNAPSHOT.jar com.anisa.App
